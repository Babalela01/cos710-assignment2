\documentclass[10pt,a4paper,conference]{IEEEtran}
\usepackage[margin=0.5in]{geometry}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{multirow}
\usepackage{cite}
\usepackage{verbatim}
\DeclareGraphicsExtensions{.png}
\graphicspath{
	{../data/}
}
	
\usepackage{chngcntr}
\newcommand{\tablefolder}{../data1/}

\begin{document}

\title{Generation Gap Genetic Algorithms \\
\hfil	\\
\large COS710 Assignment 2}
\author{Renette Ros 13007557}
\date{\today}

\maketitle
%\tableofcontents
%\pagebreak
\section{Introduction}
Genetic Algorithms (GAs) are algorithmic models to simulate genetic systems \cite{apengel} with each possible solution being a chromosome in the population.   

This report compares the performance of Generational Genetic Algorithms (GGAs) with and without elitism and Steady State Genetic Algorithms  (SSGAs) with different replacement strategies on floating point benchmark functions. 

The simulations performance is measured by how quickly the GA converges and what the average fitness of the generation is. 

 \section{Background}
The traditional GA as proposed by Holland \cite{holland} is a GGA that uses a bitstring representation with fitness proportional selection, 1 point crossover and uniform mutation. At every generation, the algorithm selects a new population by applying selection, crossover and mutation to the original population. 

The SSGA uses a replacement strategy to make a decision of whether the parent or offspring survives directly after offspring is created. This creates a larger generation gap. \cite{apengel}

In this assignment, seven floating point benchmark functions with different characteristics (uni-modal/separable/non-separable) are used to evaluate each GA. 
 
\section{Implementation}
A custom implementation in Java was used to run the simulations. 
 
\subsection{The Problems}
Seven different floating point functions (two uni-modal and five multi-modal) were minimized in 30 dimensions to evaluate the different GAs.

\subsubsection{Uni-modal, Separable Problems}
\hfill \\
\textbf{Absolute value} 
\[f_{abs}(x) = \sum_{i=1}^{n_x}\left|x_i\right|\]
\[ x \in [-100, 100]\]
\textbf{Spherical Function}
 \[f_{sphe} = \sum_{i=1}^{n_x} x^2_i\]
 \[x \in [-5.12, 5.12] \]

\subsubsection{Multi-modal, Non-separable Problems}
\hfill \\
\textbf{Ackley Function}
 \[\begin{split}
 f_{ack}(x) = -20 e^{-0.2 \sqrt{\frac{1}{n} \sum_{i=1}^{n_x} x_i^2}}  - \\ e^{\frac{1}{n}\sum_{i=1}^{n_x} cos(2\pi x_i))}
    + 20 + e \end{split}\]
 \[x \in [-32.786, 32.786] \]
\textbf{Griewank Function}
 \[f_{gri}(x) = 1 + \frac{1}{4000} \sum_{i=1}^{n_x} x^2_i - 
 \prod_{i=1}^{n_x} cos(\frac{x_i}{\sqrt{i}})\]
 \[ x \in [-600, 600]\]
\textbf{Salomon Function}
 \[f_{sol}(x) = -cos(2 \pi \sum_{i=1}^{n_x} x^2_i ) + 0.1 \sqrt{\sum_{i=1}^{n_x} x^2_i} + 1\]
 \[x \in [-100, 100] \]
 
 \subsubsection{Multi-modal, Separable Problems}
 \hfill \\
 \textbf{Alpine function}
\[f_{alp}(x) = \prod_{i=1}^{n_x} \sqrt{x_i}sin(x_i)\]
\[x \in [0,10]\] 
 \textbf{Rastrigin Function}
 \[f_{ras}(x) = = 10n + \sum_{i=1}^{n_x} (x_i^2 -10cos(2\pi x_i)),\]
 \[x \in [-5.12, 5.12] \]
 

\subsection{Simulation parameters}

\subsubsection{Boundary Violations}
Chromosome dimensions that violate boundary constraints are clamped to the minimum or maximum constraint for that problem. This strategy was chosen to keep corrected values close to the original values. 

\subsubsection{Selection Strategy}
Rank based selection is used with Roulette wheel sampling. Rank based selection prevents one good individual from dominating the entire selection \cite{apengel} reducing exploration. Chromosomes with the best (smallest) fitness has the highest probability of being selected. 

\subsubsection{Crossover}
The crossover strategy used is Simulated Binary Crossover (SBX). SBX tries to mimic crossover from binary chromosomes and it not create bias towards any of the parents. \cite{apengel}
SBX uses two parents to generate two offspring with the following equations:

\begin{gather*}
\tilde{x}_{ 1j } = 0.5 [ (1 + y_j)x_{1j}(t) + (1 - y_j)x_{2j}(t) ]  \\
\tilde{x}_{ 2j } = 0.5 [ (1 - y_j)x_{1j}(t) + (1 + y_j)x_{2j}(t) ] 
\end{gather*}
\begin{gather*}
\text{with}  y_j = \begin{cases}
(2r_j)^{1 \over n + 1} \text{  if  } r_j \le 0.6 \\
({1 \over 2(1-r_j)})^{1 \over n + 1}  \text{  otherwise }\\
\end{cases}
\text { and } n = 1
\end{gather*} 

A crossover rate of 75\% is used to ensure enough exploration.  

\subsubsection{Mutation}
The genetic algorithm uses Uniform floating-point mutation: 
\begin{gather*}
x_{ij}' = \begin{cases}
\tilde{x}_{ij}(t) + \delta(t,\tilde{x}_{max,j} - \tilde{x}_{ij}(t)) \text{if a random digit is 0} \\
\tilde{x}_{ij}(t) + \delta(t,\tilde{x}_{ij} - \tilde{x}_{min,j}(t)) \text{if a random digit is 1} \\
\end{cases}
\end{gather*} \cite{apengel}

Because a good mutation rate is very hard to choose, the mutation rate is reduced over time according to the formula 
\begin{displaymath}
	p_m(t) = {1 \over 240} + {0.11375 \over 2^t}
\end{displaymath}
as proposed by Fogarty \cite{fogarty}

\subsubsection{Replacement Strategies}
The three replacement strategies investigated are:
\begin{itemize}
	\item Replace worst - The two offspring replaces the two worst elements in the population.
	\item Replace parents - The best 2 chromosomes from the parents and the children replaces the parents.
	\item Replace random - Two random elements in the generation is replaced. 
\end{itemize}
For each of these replacement strategies a number of offspring equal to the population size is created and considered.

\subsubsection{Elitism}
One of the GGA's does not have any elitism. The second GGA uses 1\% elitism. The low percentage is chosen to prevent too early convergence. 

\subsubsection{Population}
The population consists of 150 chromosomes that are initialized randomly inside the problem dimensions. The simulation is run up to a maximum of 7000 simulations. 

\subsection{Performance Measures}
\begin{itemize}
	\item \textbf{Convergence time} Time to converge to a possible solution
	\item \textbf{Fitness} The average fitness at generation 7000
\end{itemize}


\subsubsection{Statistical Test}
Every algorithm is run 50 times for every problem. A Mann-U-Whitney test is used to compare the results of each algorithm to the GGA with and without elitism. The populations compared by the Mann-U-Whitney test is the average fitnesses of the 50 runs at generation 7000 of the GA. The results show that most of the GAs converges before 7000 generations. 

\section{Research Results}

\subsection{Absolute}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Absolute_graph"}
	\caption{Fitness over time for Absolute function}
	\label{fig:abs}
\end{figure}

\begin{table}[!ht]
	\centering
	\input{./average/Absolute_avg.tex}
	\caption{Absolute function: Average fitness at generation 7000}
\end{table}

As illustrated by Figure \ref{fig:abs}, the SSGAs that replace the parents or worst elements converges faster than the traditional GA. The GGA that uses elitism converges at a worse fitness than all the other GAs. A Mann-U-Whitney test on the populations at generation 7000 shows that the difference between the normal GGA and all the other GAs is significant, but that the difference between the SSGAs is not significant. 

\subsection{Ackley}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Ackley_graph"}
	\caption{Fitness over time for Ackley function}
	\label{fig:ack}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Ackley_avg.tex}
	\caption{Ackley function: Average fitness at generation 7000}
\end{table}

With the Ackley function the SSGAs that replace the parents and the worst elements converge faster than the other GAs. A Mann-U-Whitney test shows that the differences between the averages of all algorithms at generation 300 is significant. 

A probable cause for the bad average fitness found by the "worst chromosome" replacement strategy  is that it gets stuck in local optima. 

\subsection{Alpine}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Alpine_graph"}
	\caption{Fitness over time for Alpine function}
	\label{fig:alp}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Alpine_avg.tex}
	\caption{Alpine function: Average fitness at generation 7000}
\end{table}

With the alpine function, the SSGAs that replaces the parents and the worst chromosomes finds the best average fitness. A Mann-U-Whitney test on the results show that the differences between all the GAs are significant.

\subsection{Griewank}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Griewank_graph"}
	\caption{Fitness over time for Griewank function}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Griewank_avg.tex}
	\caption{Griewank function: Average fitness at generation 7000}
\end{table}
In the Griewank function the GGA with elitism also converges at a worse optimum than the other algorithms. The SSGAs that replace the worst and the parents converges the fastest, but their a Mann-U-Whutney test shows that the fitness differences between the normal GGA and the SSGAs are not significant.

\subsection{Rastrigin}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Rastrigin_graph"}
	\caption{Fitness over time for Rastrigin function}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Rastrigin_avg.tex}
	\caption{Rastrigin Function: Average fitness at generation 7000}
\end{table}
In the Rastrigin function neither of the GGAs converges to a good solution. The SSGAs that replaces a random chromosome took longer to converge, but converged to a better optimum. A Man-U-Whitney test shows that the difference between the GGAs and the SSGAs is significant, but that the difference between the fittest two SSGAs  - the random and worst replacement strategies - is insignificant. 


\subsection{Salomon}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Salomon_graph"}
	\caption{Fitness over time for Salomon function}
	\label{fig:sal}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Griewank_avg.tex}
	\caption{Salomon function: Average fitness at generation 7000}
\end{table}

As shown by Figure \ref{fig:sal} the SSGA's with worst chromosome and parent replacement have less exploration than the other algorithms. The random replacement does not converge as quickly as the other SSGAs. A Mann-U-Whitney test shows that the difference in average fitness between the algorithms are significant. 

\subsection{Spherical}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\linewidth]{"Spherical_graph"}
	\caption{Fitness over time for Spherical function}
\end{figure}
\begin{table}[!ht]
	\centering
	\input{./average/Spherical_avg.tex}
	\caption{Spherical function: Average fitness at generation 7000}
\end{table}

The spherical function yielded results very similar result to the absolute function which is also uni-modal. The GGA with elitism takes longer to converge and converges at a worse fitness than the other algorithms. A Man-U-Whitney test performed on the average fitness at generation 7000 shows that there is a significant difference between the Elitism GGA and  the other algorithms, but not between the normal GGA and the different SSGAs.  

\section{Conclusions}
  
On the uni-modal problems, the different GAs had very similar performance.

On the multi-modal non-separable problems, the SSGAs with the parent and random replacement strategies found better solutions than the worst chromosome replacement strategy. The worst chromosomes strategy could cause the SSGA to get stuck in local optima due to the amount of children being generated and the selection strategy selecting the best chromosomes. 

For the two multi-modal separable problems the SSGAs with the worst and parent replacement strategies performed the best in terms of average fitness.

On all the tested problems the GGA with elitism performed worse in terms of average fitness than the normal GGA and the SSGA, and the SSGAs with the parent and worst element replacement strategies converged the fastest. The GA that produces the fittest results is very problem dependent. 
\bibliography{bibliography}
\bibliographystyle{plain}


\end{document}
