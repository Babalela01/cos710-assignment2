import itertools
from scipy.stats import mannwhitneyu


def prods(arr):
    for i in range(len(arr[:-1])):
        a = arr[i]
        for b in arr[i+1:]:
            yield (a,b)

ggas = ["gga",
         "gga-elitism",
         "ssga-parent",
         "ssga-random",
         "ssga-worst"]

probs = ["Absolute",
         "Alpine",
         "Ackley",
         "Griewank",
         "Rastrigin",
         "Salomon",
         "Spherical"]


def mu(x1, x2):
    muw = mannwhitneyu(x1, x2)
    p = muw[1] * 2
    return p


def process(x):
    return float(x.split(",")[-1].strip())


for prob in probs:
    print("Testing ", prob)
    data = {}
    for f in ggas + ggas:
        with open("./dataA/%s/%s" % (prob, f + "_mannu.csv")) as file:
            readlines = file.readlines()
            data[f] = [process(line) for line in readlines]

    line = r" {} & {}  & {}  \\ " + "\n"

    def getline(k1, k2):
        print("\tKeys: ", k1, k2)
        try:
            value = mu(data[k1], data[k2])
            return line.format(k1, k2, value < 0.05)
        except ValueError:
            return line.format(k1, k2, False)

    with open("./dataA/%s_%s.tex" % (prob, "manuResults"), "w") as outfile:
        outfile.write(r"\begin{tabular} {l l r}")
        outfile.write(r"\textbf{Problem 1} & \textbf{Problem 2} & \textbf{Significant} \\ & & (p \textless 0.05) \\" + "\n")
        outfile.write(r"\hline " + "\n")
        #outfile.write(getline(ggas[0], ggas[1]))
        outfile.writelines(getline(g, s) for g, s in prods(ggas))

        outfile.write(r"\end{tabular}" + "\n")



