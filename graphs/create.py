# data = [{name: ..., data: ..., settings: ...}]

import matplotlib.pyplot as plt


# matplotlib.rc('font', family='Arial')

def generations_graph(dataAvg, dataBest, title, file, cutoff):
    plt.gca().set_prop_cycle(None)
    plt.clf()
    x = range(cutoff)

    plt.gca().set_prop_cycle(None)
    for seriesname in dataAvg:
        series = dataAvg[seriesname]
        y = series['data'][:cutoff]
        settings = '-'
        label = seriesname
        plt.plot(x, y, settings, label=label)

    """ plt.gca().set_prop_cycle(None)
    for seriesname in dataBest:
        series = dataBest[seriesname]
        y = series['data'][:cutoff]
        settings = '.'
        label = seriesname
        plt.plot(x, y, settings, label=label)
    """

    plt.xlabel('Time (generation)')
    plt.ylabel('Fitness')
    plt.title(title)
    plt.legend(loc='best')
    plt.savefig(file)
    plt.clf()


files = ["gga-elitism_bestavg.csv",
         "gga_bestavg.csv",
         "ssga-parent_bestavg.csv",
         "ssga-random_bestavg.csv",
         "ssga-worst_bestavg.csv"]

probs = {"Absolute": 2000,
         "Alpine": 6999,
         "Ackley": 4000,
         "Griewank": 4000,
         "Rastrigin": 4000,
         "Salomon": 4000,
         "Spherical": 4000}

ITERATIONS = 6999

outfile = "graph.png"
# series : [arr]
cutoff = ITERATIONS
settings1 = "."
settings2 = '-'

for prob in probs:
    dataA = {}
    dataB = {}
    title = prob + " - Fitness over time"
    for file in [open("./data/%s/%s" % (prob, f)) for f in files]:
        lines = file.readlines()
        best = []
        avg = []

        for line in lines[1:]:
            splitted = line.strip().split(",")
            best.append(float(splitted[1]))
            avg.append(float(splitted[2]))
        n = file.name.split("/")[-1].split("_")[0]
        bestd = {"data": best, "settings": settings1}
        avgd = {"data": avg, "settings": settings2}
        dataB[n + " best"] = bestd
        dataA[n + " average"] = avgd
    cutoff = probs[prob]
    generations_graph(dataA, dataB, title, "data/%s_graph.png" % prob, cutoff)
