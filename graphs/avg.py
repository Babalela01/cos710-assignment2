import statistics
from scipy.stats import mannwhitneyu

gas = ["gga",
         "gga-elitism",
         "ssga-parent",
         "ssga-random",
         "ssga-worst"]

probs = ["Absolute",
         "Alpine",
         "Ackley",
         "Griewank",
         "Rastrigin",
         "Salomon",
         "Spherical"]

def process(x):
    return float(x.strip())


for prob in probs:
    print("Testing ", prob)
    data = {}
    for f in gas:
        with open("./data/%s/%s" % (prob, f + "_mannu.csv")) as file:
            readlines = file.readlines()
            data[f] = [process(line) for line in readlines[1:]]

    line = r" {} & {:.4f} & {:.4f} \\ " + "\n"

    def getline(k1):
        print("\tKeys: ", k1)
        return line.format(k1, statistics.mean(data[k1]), statistics.stdev(data[k1]))

    with open("./data/%s_%s.tex" % (prob, "avg"), "w") as outfile:
        outfile.write(r"\begin{tabular} {l r r}")
        outfile.write(r"\textbf{GA} & \textbf{Average Fitness} & \textbf{Std Deviation} \\" + "\n")
        outfile.write(r"\hline " + "\n")
        #outfile.write(getline(ggas[0], ggas[1]))
        outfile.writelines(getline(g) for g in gas)
        outfile.write(r"\hline " + "\n")
        outfile.write(r"\end{tabular} \\ " + "\n")
        outfile.write(r"\hfill \\" + "\n")



