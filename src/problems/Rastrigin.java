package problems;

import java.util.Collection;
import java.util.function.BinaryOperator;

/**
 * @author renet
 *         on 2016/03/28.
 */
public class Rastrigin  extends DoubleProblem  {
    public Rastrigin() {
        super(new Double[]{-5.12, 5.12});
    }

    @Override
    public Double evaluate(Collection<Double> values) {

        BinaryOperator<Double> func = (t, x) -> t + x * x - 10.0 * Math.cos(2.0 * Math.PI * x);

        double tmp = values.stream().reduce(0.0, func::apply);
        return 10 * values.size() + tmp;
    }


}
