package problems;

import java.util.Collection;

/**
 * @author renet
 *         on 2016/03/28.
 */
public class Ackley extends DoubleProblem  {
    public Ackley() {
        super(new Double[]{-32.768, 32.768});
    }

    @Override
    public Double evaluate(Collection<Double> values) {
        double a = 20.0;
        double b = 0.2;
        double c = 2.0 * Math.PI;
        int d = values.size();

        Double sum_square = values.stream().reduce(0.0, (t, v) -> t + v*v);
        Double sum_cos = values.stream().reduce(0.0, (t, x) -> t + Math.cos(c * x));

        double term1 = -a * Math.exp(-b * Math.sqrt(sum_square / d));
        double term2 = -Math.exp(sum_cos / d);
        return a + Math.exp(1) + term1 + term2;
    }

}
