package problems;

import java.util.Collection;
import java.util.function.BinaryOperator;

/**
 * @author renet
 *         on 2016/03/28.
 */
public class Griewank  extends DoubleProblem  {
    public Griewank() {
        super(new Double[]{-600.0, 600.0});
    }

    @Override
    public Double evaluate(Collection<Double> values) {

        BinaryOperator<Double> func = (t, x) -> t + Math.abs(x);
        double sumsq = values.stream().reduce(0.0, func::apply);
        double prod = 1;

        int i =0;
        for (double x : values) {
            prod *= Math.cos(x / Math.sqrt(i + 1));
            ++i;
        }

        return 1 + sumsq / 4000.0 - prod;
    }


}
