package problems;

import java.util.Collection;
import java.util.function.BinaryOperator;

/**
 * @author renet
 *         on 2016/03/28.
 */
public class Absolute extends DoubleProblem  {
    public Absolute() {
        super(new Double[]{-100.0, 100.0});
    }

    @Override
    public Double evaluate(Collection<Double> values) {

        BinaryOperator<Double> func = (t, x) -> t + Math.abs(x);

        return values.stream().reduce(0.0, func::apply);
    }


}
