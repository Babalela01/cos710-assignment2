package problems;

import java.util.Collection;

/**
 * @author renet
 *         on 2016/03/28.
 */
public class Salomon  extends DoubleProblem  {
    public Salomon() {
        super(new Double[]{-100.0, 100.0});
    }

    @Override
    public Double evaluate(Collection<Double> chromosome) {

        double sqrt = Math.sqrt( chromosome.stream().reduce(0.0, (t, v) -> t + v*v) );
        double term1 = -Math.cos(2 * Math.PI * sqrt);
        double term2 = 0.1 * sqrt;
        return term1 + term2 + 1;
    }

}
