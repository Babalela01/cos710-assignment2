package problems;

import ga.Chromosome;

/**
 * @author renet
 *         on 2016/03/31.
 *
 */
public abstract class DoubleProblem implements Problem<Double> {
    private final Double[] boundaries;

    protected DoubleProblem(Double[] boundaries) {
        this.boundaries = boundaries;
    }

    public boolean violatesBoundaries(Chromosome<Double> chromosome) {
        Double[] boudaries = boundaries();
        for (Double value : chromosome.getChromosome()) {
            if (value < boudaries[0] || value > boudaries[1])
                return true;
        }
        return false;
    }

    public Chromosome<Double> evaluateAndSet(Chromosome<Double> chromosome) {
        chromosome.clamp(boundaries);
        if (! violatesBoundaries(chromosome))
            chromosome.setFitness(evaluate(chromosome));
        return chromosome;
    }
    public Double[] boundaries() {
        return boundaries;
    }
}
