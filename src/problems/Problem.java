package problems;

import ga.Chromosome;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author renet
 *         on 2016/03/28.
 */
public interface Problem<T extends Comparable<T>> {

    default Double evaluate(Chromosome<T> chromosome) {
        return evaluate(Arrays.asList(chromosome.getChromosome()));
    }

    Double evaluate(Collection<T> values);

    Chromosome<T> evaluateAndSet(Chromosome<T> chromosome);

    T[] boundaries();

    boolean violatesBoundaries(Chromosome<T> chromosome);
}
