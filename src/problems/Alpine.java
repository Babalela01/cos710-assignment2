package problems;

import java.util.Collection;

/**
 * @author renet
 *         on 2016/04/02.
 */
public class Alpine extends DoubleProblem {
    public Alpine() {
        super(new Double[]{0.0, 10.0});
    }

    @Override
    public Double evaluate(Collection<Double> values) {
        double prod = 1.0;
        for (Double d : values)
            prod *= Math.sqrt(d) * Math.sin(d);
        return prod;
        //return values.stream().reduce(1.0, (t, x) -> t * (Math.sqrt(x) * Math.sin(x)));
    }
}
