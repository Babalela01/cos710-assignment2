import stopping.MaxGenerations;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


/**
 * Created by Renette on 2015-04-29.
 * This class consolidates all simulation csv files in a specified folder to 3 csv files - best, average and bestTraders
 */
public class FolderConsolidator {

    public static void consolidateFolder(File folder) throws IOException {
        if (!folder.exists() || !folder.isDirectory())
            throw new IOException(folder.getPath() + " is not a directory.");

        File[] list = folder.listFiles();
        if (list == null || list.length == 0) {
            throw new IOException("No input files in " + folder.getPath());
        }

        ArrayList<Double> bestTotals = new ArrayList<>(Collections.nCopies(Main.MAX_GENERATIONS, 0.0));
        ArrayList<Double> averageTotals = new ArrayList<>(Collections.nCopies(Main.MAX_GENERATIONS, 0.0));
        ArrayList<String> mannu  = new ArrayList<>(Main.RUN_COUNT);

        for (File f: list) {
            String name = f.getName();

            BufferedReader reader = new BufferedReader(new FileReader(f));
            reader.readLine();

            String line = reader.readLine();
            int generationCounter = 1;
            /**
             * Append to existing lines
             */
            while (line != null && generationCounter <  Main.MAX_GENERATIONS) {
                if (!line.isEmpty()) {
                    String[] parts = line.split(",");

                    double best = Double.valueOf(parts[1]);
                    Double average = Double.valueOf(parts[2]);

                    double bestTotal = bestTotals.get(generationCounter);
                    double averageTotal = averageTotals.get(generationCounter);
                    averageTotals.set(generationCounter, averageTotal + average);
                    bestTotals.set(generationCounter, bestTotal + best);

                    ++generationCounter;
                    if (generationCounter == Main.MAX_GENERATIONS-1) {
                        mannu.add(average.toString());
                    }
                }
                line = reader.readLine();
            }
        }

        File manfile = new File(folder.getParent() + "/" + folder.getName() + "_mannu.csv");
        BufferedWriter writer = new BufferedWriter(new FileWriter(manfile));
        String[] strings = new String[mannu.size()];
        strings = mannu.toArray(strings);
        writer.write(String.join("\n", strings));
        writer.close();

        File outfile = new File(folder.getParent() + "/" + folder.getName() + "_bestavg.csv");
        BufferedWriter averages = new BufferedWriter(new FileWriter(outfile));

        /**
         * Write constructed liens to file with generatin numbers.
         */
        averages.write("Generation, Best, Average\n");

        for (int i = 1; i< Main.MAX_GENERATIONS; ++i) {
            String pattern = "%d, %.5f, %.5f%n";
            averages.write(String.format(pattern, i,
                    bestTotals.get(i)/Main.RUN_COUNT,
                    averageTotals.get(i)/Main.RUN_COUNT));
        }

        averages.close();
    }

    public static void main(String args[]) throws IOException {
        File outfolder = new File("data");
        for (File probdir : outfolder.listFiles()) {
            if (probdir.isDirectory())
                for(File gadir : probdir.listFiles()) {
                    if (gadir.isDirectory())
                        consolidateFolder(gadir);
                }
        }
    }

}
