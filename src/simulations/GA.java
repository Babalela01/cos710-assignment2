package simulations;

import crossover.Crossover;
import ga.Chromosome;
import ga.Generation;
import ga.InitializeFunction;
import mutation.Mutator;
import problems.Problem;
import selection.Selection;
import stopping.StoppingCondition;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Renette on 2015-04-09.
 * Abstract class GA and HC simulations inherit from.
 */

public abstract class GA<T extends Comparable<T>> {
    protected final Problem<T> problem;
    protected final StoppingCondition stop;
    protected final Selection<T> selector;
    protected final Crossover<T> crossoverStrategy;
    protected final Mutator<T> mutator;

    protected long generationCounter = 0;
    protected final int populationSize;
    protected final double crossoverProbability;
    protected double mutationRate;
    protected final int maxGenerations;

    public GA(Problem<T> problem, StoppingCondition s, int populationSize, double mutationRate, Mutator<T> mutator, Crossover<T> crossoverStrategy, int maxGenerations, Selection<T> selectionStrategy, double crossoverProbability) {
        this.problem = problem;
        stop = s;
        this.populationSize = populationSize;
        this.mutationRate = mutationRate;
        this.mutator = mutator;
        this.crossoverStrategy = crossoverStrategy;
        this.maxGenerations = maxGenerations;
        this.selector = selectionStrategy;
        this.crossoverProbability = crossoverProbability;
    }

    public long getGenerationCounter() {
        return generationCounter;
    }

    public void runSimulations(int runCount, String filePattern, InitializeFunction<T> func, int dimensions) throws IOException {
        for (int i = 0; i < runCount; ++i) {
            SimulationStats stats = simulate(func, dimensions);
            String filePath = String.format(filePattern, String.format("%04d", (i + 1)));
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(stats.toString());
            writer.close();
            System.out.printf("%s %d complete%n",this.getClass().getSimpleName(), i + 1);
        }
    }

    protected SimulationStats simulate(InitializeFunction<T> func, int dimensions) {

        generationCounter = 0;
        Generation<T> current = new Generation<>(populationSize, dimensions, func);
        current.evaluate(problem);
        SimulationStats stats = new SimulationStats(maxGenerations, current);

        while (!stop.mustStop(this)) {
            ++generationCounter;
            mutationRate = 1/240 + 0.11375/Math.pow(2, generationCounter);
            Generation<T> next = selectAndEvalGeneration(current);

            stats.add(next);
            current = next;
        }

        return stats;
    }

    protected abstract Generation<T> selectAndEvalGeneration(Generation<T> current);

    public class SimulationStats {
        private final ArrayList<Double> generationAverages;
        private final ArrayList<Double> generationBests;

        public SimulationStats(int maxGenerations, Generation<T> gen) {
            generationAverages = new ArrayList<>(maxGenerations);
            generationAverages.add(gen.getAverage());

            Chromosome best = gen.getBest();

            generationBests = new ArrayList<>(maxGenerations);
            generationBests.add(best.getFitness());
        }

        public void add(Generation<T> g) {
            Chromosome best = g.getBest();
            generationAverages.add(g.getAverage());
            generationBests.add(best.getFitness());
        }

        public String toString() {
            StringBuilder body = new StringBuilder("Generation, Best Fitness, Average Fitness\n");
            int genCount = generationAverages.size();
            for (int i = 0; i < genCount; ++i) {
                body.append(String.format("%d, %.4f, %.4f %n", (i + 1), generationBests.get(i), generationAverages.get(i)));
            }
            return body.toString();
        }

    }
}
