package simulations;

import crossover.Crossover;
import ga.Chromosome;
import ga.Generation;
import selection.Selection;
import stopping.MaxGenerations;
import mutation.Mutator;
import problems.Problem;

import java.util.List;


/**
 * Created by Renette on 2015-04-09.
 * Genetic Algorithm simulation
 */
public class GGA<T extends Comparable<T>> extends GA<T> {
    private final int elitismCount;

    public GGA(Problem<T> problem,
               Selection<T> selectionStrategy,
               Crossover<T> crossoverStrategy,
               Mutator<T> mutator,
               int maxGenerations,
               int populationSize,
               double crossoverProbability,
               double mutationRate,
               double elitismPercent) {

        super(problem, new MaxGenerations(maxGenerations), populationSize, mutationRate, mutator, crossoverStrategy, maxGenerations, selectionStrategy, crossoverProbability);
        this.elitismCount = (int) (elitismPercent /100 * populationSize);
    }

    @Override
    protected Generation<T> selectAndEvalGeneration(Generation<T> current) {
        selector.setGeneration(current);
        Generation<T> next = new Generation<>(populationSize);

        for (int i = 0; i < elitismCount; ++i) {
            next.add(current.get(i).clone());
        }

        T[] boundaries = problem.boundaries();
        while (next.size() < populationSize) {
            /**
             * Clone the selected Chromosomes to prevent mutation of existing Chromosomes.
             * Clone stores the last fitness os there's no need to re-evaluate
             */
            Chromosome<T> a = selector.select().clone();
            Chromosome<T> b = selector.select().clone();
            double r = Math.random();

            if (r < crossoverProbability) {
                List<Chromosome<T>> children = crossoverStrategy.cross(a, b);
                a = children.get(0);
                b = children.get(1);
            }
            mutator.mutate(a, mutationRate);
            mutator.mutate(b, mutationRate);

            problem.evaluateAndSet(a);
            problem.evaluateAndSet(b);

            next.add(a);
            next.add(b);
        }

        return next;
    }
}