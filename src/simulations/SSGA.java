package simulations;

import crossover.Crossover;
import ga.Generation;
import mutation.Mutator;
import problems.Problem;
import replacement.ReplacementStrategy;
import selection.Selection;
import stopping.MaxGenerations;

/**
 * @author renet
 *         on 2016/03/29.
 */
public class SSGA<T extends Comparable<T>> extends GA<T> {
    private final ReplacementStrategy<T> replacementStrategy;

    public SSGA(Problem<T> problem,
                Selection<T> selectionStrategy,
                Crossover<T> crossoverStrategy,
                Mutator<T> mutator,
                int maxGenerations,
                int populationSize,
                double crossoverProbability,
                double mutationRate,
                ReplacementStrategy<T> replacementStrategy) {

        super(problem, new MaxGenerations(maxGenerations), populationSize, mutationRate, mutator, crossoverStrategy,
                maxGenerations, selectionStrategy, crossoverProbability);
        this.replacementStrategy = replacementStrategy;
        this.replacementStrategy.setVariables(crossoverStrategy, mutator, selectionStrategy, problem);
    }

    @Override
    protected Generation<T> selectAndEvalGeneration(Generation<T> current) {
        return replacementStrategy.selectAndReplace(current, crossoverProbability, mutationRate);
    }

}
