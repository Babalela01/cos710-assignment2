package replacement;

import ga.Chromosome;
import ga.Generation;

import java.util.List;

/**
 * Created by Renette on 2015-04-10.
 * Performs tournamnet selection on a generation
 */
public class ReplaceParent<T extends Comparable<T>> extends ReplacementStrategy<T> {

    @Override
    public Generation<T> selectAndReplace(Generation<T> generation, double crossoverRate, double mutationRate) {

        this.selectionStrategy.setGeneration(generation);
        int populationSize = generation.size();

        List<Chromosome<T>> values = generation.getMembers();

        for (int i=0; i<populationSize/2; ++i) {
            Chromosome<T> parent1 = this.selectionStrategy.select();
            Chromosome<T> parent2;
            do {
                parent2 = this.selectionStrategy.select();
            } while (parent1 == parent2);

            List<Chromosome<T>> chromosomes = crossAndMutate(parent1.clone(), parent2.clone(), crossoverRate, mutationRate);

            chromosomes.add(parent1);
            chromosomes.add(parent2);

            chromosomes.sort(Generation.comparator);

            int index = values.indexOf(parent1);
            values.set(index, chromosomes.get(0));
            index = values.indexOf(parent2);
            values.set(index, chromosomes.get(1));

            generation.sort();

        }
        return generation;
    }
}
