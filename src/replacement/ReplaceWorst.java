package replacement;

import ga.Chromosome;
import ga.Generation;

import java.util.List;

/**
 * @author renet
 *         on 2016/03/29.
 */
public class ReplaceWorst<T extends Comparable<T>> extends ReplacementStrategy<T> {

    @Override
    public Generation<T> selectAndReplace(Generation<T> generation,
                                          double crossoverProbability,
                                          double mutationRate) {
        this.selectionStrategy.setGeneration(generation);
        int populationSize = generation.size();
        List<Chromosome<T>> values = generation.getMembers();
        int popSize = values.size();

        for (int i=0; i<populationSize/2; ++i) {
            /**
             * Clone the selected Chromosomes to prevent mutation of existing Chromosomes.
             * Clone stores the last fitness os there's no need to re-evaluate
             */
            Chromosome<T> a = this.selectionStrategy.select().clone();
            Chromosome<T> b = this.selectionStrategy.select().clone();

            List<Chromosome<T>> chromosomes = crossAndMutate(a, b, crossoverProbability, mutationRate);
            values.remove(popSize-1);
            values.remove(popSize-2);
            values.addAll(chromosomes);

            generation.sort();
        }

        return generation;
    }
}
