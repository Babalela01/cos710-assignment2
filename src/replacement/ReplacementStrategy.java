package replacement;

import crossover.Crossover;
import ga.Chromosome;
import ga.Generation;
import mutation.Mutator;
import problems.Problem;
import selection.Selection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author renet
 *         on 2016/03/29.
 */
public abstract class ReplacementStrategy<T extends Comparable<T>> {
    protected Selection<T> selectionStrategy;
    protected Mutator<T> mutationStrategy;
    protected Crossover<T> crossoverStrategy;
    protected Problem<T> problem;

    public void setVariables(Crossover<T> crossoverStrategy, Mutator<T> mutator, Selection<T> selector,
                             Problem<T> problem) {
        this.crossoverStrategy = crossoverStrategy;
        this.mutationStrategy = mutator;
        this.selectionStrategy = selector;

        this.problem = problem;
    }

    protected int rand(int popSize) {
        return (int) (Math.random() * popSize);
    }

    public abstract Generation<T> selectAndReplace(Generation<T> oldGen,
                                                   double crossoverRate,
                                                   double mutationRate);


    protected List<Chromosome<T>> crossAndMutate(Chromosome<T> parent1, Chromosome<T> parent2, double crossoverProbability, double mutationRate) {
        double r = Math.random();
        if (r < crossoverProbability) {
            List<Chromosome<T>> children = crossoverStrategy.cross(parent1, parent2);
            parent1 = children.get(0);
            parent2 = children.get(1);
        }
        mutationStrategy.mutate(parent1, mutationRate);
        mutationStrategy.mutate(parent2, mutationRate);

        problem.evaluateAndSet(parent1);
        problem.evaluateAndSet(parent2);

        ArrayList<Chromosome<T>> chromosomes = new ArrayList<>(2);
        chromosomes.add(parent1);
        chromosomes.add(parent2);
        return chromosomes;
    }
}
