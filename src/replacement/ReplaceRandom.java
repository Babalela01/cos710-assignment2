package replacement;

import ga.Chromosome;
import ga.Generation;

import java.util.List;

/**
 * @author renet
 *         on 2016/03/29.
 */
public class ReplaceRandom<T extends Comparable<T>> extends ReplacementStrategy<T> {

    @Override
    public Generation<T> selectAndReplace(Generation<T> generation,
                                          double crossoverProbability,
                                          double mutationRate) {
        this.selectionStrategy.setGeneration(generation);
        int populationSize = generation.size();

        List<Chromosome<T>> values = generation.getMembers();

        for (int i=0; i<populationSize/2; ++i) {
            /**
             * Clone the selected Chromosomes to prevent mutation of existing Chromosomes.
             * Clone stores the last fitness os there's no need to re-evaluate
             */

            List<Chromosome<T>> chromosomes = crossAndMutate(this.selectionStrategy.select().clone(), this.selectionStrategy.select().clone(), crossoverProbability, mutationRate);
            values.set(rand(populationSize), chromosomes.get(0));
            values.set(rand(populationSize), chromosomes.get(1));

            generation.sort();
        }
        return generation;
    }

}
