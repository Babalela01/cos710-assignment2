package selection;

import ga.Chromosome;
import ga.Generation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;


import org.apache.commons.math3.stat.ranking.*;

/**
 * @author Renette
 */

public class RankBasedSelection<T extends Comparable<T>> implements Selection<T> {
    private ArrayList<Double> steps;
    private List<Chromosome<T>> generation;

    @Override
    public Chromosome<T> select() {
        double r = Math.random();
        int i = 0;
        while (r > steps.get(i)) {
            ++i;
        }
        return generation.get(i);
    }

    public void setGeneration(Generation<T> generation) {
        this.generation = generation.getMembers();

        //generation.sort();
        NaturalRanking ranking = new NaturalRanking(NaNStrategy.MAXIMAL, TiesStrategy.AVERAGE);
        int size = generation.size();

        List<Double> fitness = this.generation.stream().map(Chromosome::getFitness).collect(Collectors.toList());
        Double max = fitness.get(size-1) + fitness.get(0);

        double[] fitnessprimitive = fitness.stream().mapToDouble((a) -> max - a).toArray();
        double[] ranks =  ranking.rank(fitnessprimitive);
       // double total = size * (size + 1)/2;

        this.steps = Selection.roulette(ranks);
    }

}
