package crossover;

import ga.Chromosome;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Renette
 */
@FunctionalInterface
public interface Crossover<T extends Comparable<T>> {
    default List<Chromosome<T>> cross (Chromosome<T> chromosome1, Chromosome<T> chromosome2) {
        return cross(chromosome1.getChromosome(), chromosome2.getChromosome())
                .stream().map(Chromosome<T>::new).collect(Collectors.toList());
    }

    List<T[]> cross(T[] chromosome1, T[] chromosome2);
}
