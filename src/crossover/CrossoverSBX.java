package crossover;

import java.util.ArrayList;
import java.util.List;

/**
 * @author renet
 *         on 2016/03/29.
 */
public class CrossoverSBX implements Crossover<Double> {
    @Override
    public List<Double[]> cross(Double[] chromosome1, Double[] chromosome2) {
        double n = 1;
        double power = 1 / (n + 1);
        int length = chromosome1.length;
        Double[] new1 = new Double[length];
        Double[] new2 = new Double[length];

        for (int i = 0; i < length; ++i) {
            double rj = Math.random();
            double yj;
            if (rj <= 0.5) {
                yj = Math.pow(2*rj, power);
            } else {
                yj = Math.pow(1/(2*(1-rj)), power);
            }
            double x1 = chromosome1[i];
            double x2 = chromosome2[i];

            double nx1 = 0.5*( (1+yj) * x1 + (1-yj) * x2);
            double nx2 = 0.5*( (1-yj) * x1 + (1+yj) * x2);

            new1[i] = nx1;
            new2[i] = nx2;
        }
        List<Double[]> list = new ArrayList<>(2);
        list.add(new1);
        list.add(new2);

        return list;
    }
}
