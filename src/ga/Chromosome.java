package ga;

/**
 * @author renet
 *         on 2016/03/29.
 */
public class Chromosome<T extends Comparable<T>> {
    private final T[] chromosome;
    private Double fitness = null;

    public Chromosome(T[] arr) {
         chromosome = arr;
    }
    public Chromosome(int length, InitializeFunction<T> init) {
        chromosome = InitializeFunction.initialize(length, init);
    }

    protected Chromosome(T[] chromosome, Double fitness) {
        this(chromosome);
        this.fitness = fitness;
    }

    public Double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public T[] getChromosome() {
        return chromosome;
    }

    public Chromosome<T> clone() {
        return new Chromosome<>(this.chromosome.clone(), this.fitness);
    }

    public String toString() {
        return String.format("Fitness %f", fitness);
    }

    public void clamp(T[] boundaries) {
        T min = boundaries[0];
        T max = boundaries[1];
        for (int i=0; i<chromosome.length; ++i) {
            T value = chromosome[i];
            if (value.compareTo(min) < 0 ) {
                chromosome[i] = min;
            } else if (value.compareTo(max) > 0){
                chromosome[i] = max;
            }
        }
    }
}
