package ga;

import java.lang.reflect.Array;

/**
 * @author renet
 *         on 2016/03/29.
 */
@FunctionalInterface
public interface InitializeFunction<T> {
    T getValue();

    static <T> T[] initialize(int length, InitializeFunction<T> func) {
        T[] array = (T[]) Array.newInstance(func.getValue().getClass(), length);

        for (int i=0; i<length; ++i) {
           array[i] = func.getValue();
        }
        return array;
    }
}
