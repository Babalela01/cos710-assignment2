package ga;

import problems.Problem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Renette
 */
public class Generation<T extends Comparable<T>> implements Iterable<Chromosome<T>> {
    public static final Comparator<Chromosome> comparator = (a, b) -> {
        Double fitnessa = a.getFitness();
        Double fitnessb = b.getFitness();
        return fitnessa == null && fitnessb == null? 0 :
                fitnessa == null ? 1 :
                fitnessb == null ? -1 :
                Double.compare(fitnessa, fitnessb);
    };
    private final int populationSize;

    public List<Chromosome<T>> getMembers() {
        return members;
    }

    private final List<Chromosome<T>> members;

    public Generation(int popSize, int chromosomeDimensions,
                      InitializeFunction<T> init) {
        this.populationSize = popSize;
        members = new ArrayList<>(popSize);

        for (int i = 0; i < popSize; ++i) {
            members.add(new Chromosome<>(chromosomeDimensions, init));
        }
    }

    public Generation(List<Chromosome<T>> members) {
        populationSize = members.size();
        this.members = members;
    }

    public Generation(int populationSize) {
        this.populationSize = populationSize;
        members = new ArrayList<>(populationSize);
    }

    public double getAverage() {
        return members.stream().mapToDouble(Chromosome::getFitness).average().getAsDouble();
    }

    public int size() {
        return members.size();
    }

    public void add(Chromosome<T> t) {
        if (members.size() < populationSize)
            members.add(t);
    }

    public void evaluate(Problem<T> problem) {
        members.forEach(problem::evaluateAndSet);
        sort();
    }

    public void sort() {
        members.sort(comparator);
    }

    public Chromosome<T> getBest() {
        return members.get(0);
    }

    public Chromosome<T> get(int i) {
        if (i < 0 || i >= members.size())
            System.out.print("");
        return members.get(i);
    }

    @Override
    public Iterator<Chromosome<T>> iterator() {
        return members.iterator();
    }

    public Generation<T> clone() {
        List<Chromosome<T>> newMembers = members.stream().map(Chromosome::clone).collect(Collectors.toList());
        return new Generation<>(newMembers);
    }
}
