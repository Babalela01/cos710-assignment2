import crossover.Crossover;
import crossover.CrossoverSBX;
import ga.InitializeFunction;
import mutation.DoubleMutator;
import mutation.Mutator;
import problems.*;
import replacement.ReplaceParent;
import replacement.ReplaceRandom;
import replacement.ReplaceWorst;
import selection.FitnessMinProportional;
import selection.RankBasedSelection;
import selection.Selection;
import simulations.GA;
import simulations.GGA;
import simulations.SSGA;

import java.io.File;
import java.io.IOException;

class Main {

    static final int MAX_GENERATIONS = 7000;
    private static final double MUTATION_RATE =  0.05;
    private static final double ELITISM_PERCENT = 1;
    private static final int POPULATION_SIZE = 150;
    private static final double CROSSOVER = 0.75;
    static final int RUN_COUNT = 50;
    private static final int PROBLEM_DIMENSIONS = 30;

    public static void main(String[] args) throws IOException {

        Problem<Double>[] problems = new Problem[] {
                new Alpine(),
                new Absolute(),
                new Ackley(),
                new Griewank(),
                new Rastrigin(),
                new Salomon(),
                new Spherical()
        };

        Selection<Double> selection = new RankBasedSelection<>();
        Crossover<Double> crossover = new CrossoverSBX();

        for (Problem<Double> prob : problems) {

            Double[] boundaries = prob.boundaries();
            Mutator<Double> mutate = new DoubleMutator(boundaries[0], boundaries[1]);

            Double diff = boundaries[1] - boundaries[0];double min = boundaries[0];
            InitializeFunction<Double> init = () ->  Math.random() * diff + min;
            String probname = prob.getClass().getSimpleName();

            /** Ga with Elistism */
            GA<Double> ga = new GGA<>(prob, selection, crossover, mutate, MAX_GENERATIONS,
                    POPULATION_SIZE, CROSSOVER, MUTATION_RATE, ELITISM_PERCENT);

            String superfolder = "data/";
            String pathname =  superfolder + probname + "/gga-elitism/";
            new File(pathname).mkdirs();
            ga.runSimulations(RUN_COUNT, pathname + "ggae-%s", init, PROBLEM_DIMENSIONS);

            /** Ga without Elitism */
            ga = new GGA<>(prob, selection, crossover, mutate, MAX_GENERATIONS,
                    POPULATION_SIZE, CROSSOVER, MUTATION_RATE, 0);

            pathname = superfolder + probname +  "/gga/";
            new File(pathname).mkdirs();
            ga.runSimulations(RUN_COUNT, pathname + "gga-%s", init, PROBLEM_DIMENSIONS);

            /** SSGA with Random Replacement */
            pathname = superfolder + probname + "/ssga-random/";
            new File(pathname).mkdirs();

            ga = new SSGA<>(prob, selection, crossover, mutate, MAX_GENERATIONS,
                    POPULATION_SIZE, CROSSOVER, MUTATION_RATE, new ReplaceRandom<>());
            ga.runSimulations(RUN_COUNT, pathname + "ssga-r-%s", init, PROBLEM_DIMENSIONS);

            /** SSGA with Worst Replacement */
            pathname = superfolder + probname + "/ssga-worst/";
            new File(pathname).mkdirs();

            ga = new SSGA<>(prob, selection, crossover, mutate, MAX_GENERATIONS,
                    POPULATION_SIZE, CROSSOVER, MUTATION_RATE, new ReplaceWorst<>());
            ga.runSimulations(RUN_COUNT, pathname + "ssga-w-%s", init, PROBLEM_DIMENSIONS);

            /** SSGA with Parent Replacement */
            pathname = superfolder + probname + "/ssga-parent/";
            new File(pathname).mkdirs();

            ga = new SSGA<>(prob, selection, crossover, mutate, MAX_GENERATIONS,
                    POPULATION_SIZE, CROSSOVER, MUTATION_RATE, new ReplaceParent<>());
            ga.runSimulations(RUN_COUNT, pathname + "ssga-p-%s", init, PROBLEM_DIMENSIONS);
        }
    }
}
