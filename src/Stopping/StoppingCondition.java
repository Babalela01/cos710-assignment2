package stopping;

import simulations.GA;

/**
 * Created by Renette on 2015-04-11.
 * Interface for all stopping conditions,
 */
@FunctionalInterface
public interface StoppingCondition {
    boolean mustStop(GA s);
}
