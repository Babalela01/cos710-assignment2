package stopping;

import simulations.GA;

public class MaxGenerations implements StoppingCondition {
    private final int maxGenerations;

    public MaxGenerations(int maxGenerations) {
        this.maxGenerations = maxGenerations;
    }

    public boolean mustStop(GA s) {
        return s.getGenerationCounter() >= maxGenerations;
    }

}

