package mutation;

import ga.Chromosome;

/**
 * @author renet
 *         on 2016/03/29.
 */
@FunctionalInterface
public interface Mutator<T extends Comparable<T>> {
    void mutate(Chromosome<T> chromosome, double mutationRate);
}
