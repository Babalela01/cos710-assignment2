package mutation;

import ga.Chromosome;

/**
 * @author renet on 2016/03/29.
 * Unifrom random mutation as proposed by Michalewicz
 */
public class DoubleMutator implements Mutator<Double> {
    private final double max;
    private final double min;

    public DoubleMutator(double min, double max) {
        this.max = max;
        this.min = min;
    }

    private double rand(double x) {
        return Math.random()*x;
    }

    @Override
    public void mutate(Chromosome<Double> chromosome, double mutationRate) {
        Double[] values = chromosome.getChromosome();
        for (int i=0; i < values.length; i++) {
            if (Math.random() < mutationRate) {
                double xij = values[i];
                boolean digit = Math.random() > 0.5;
                if (!digit)
                    values[i] = xij + rand(max - xij);
                else
                    values[i] = xij - rand(xij - min);

            }
        }
    }
}
