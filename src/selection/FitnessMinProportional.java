package selection;

import ga.Chromosome;
import ga.Generation;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Renette
 * Fitness proportional selction for minimization problems
 */
public class FitnessMinProportional<T extends Comparable<T>> implements Selection<T> {
    private ArrayList<Double> steps;
    private Generation<T> generation;
    @Override
    public Chromosome<T> select() {
        double r = Math.random();
        int i = 0;
        while (r > steps.get(i)) {
            ++i;
        }
        return generation.get(generation.size() - i - 1);
    }

    public void setGeneration(Generation<T> generation) {
        this.generation = generation;
        List<Chromosome<T>> members = generation.getMembers();
        double max = members.get(members.size()-1).getFitness() + members.get(0).getFitness();
        Stream<Double> values = members.stream().map(Chromosome::getFitness);

        double[] primitives = values.mapToDouble((a) -> max - a).toArray();

        this.steps = Selection.roulette(primitives);
    }
}
