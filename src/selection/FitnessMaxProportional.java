package selection;

import ga.Chromosome;
import ga.Generation;

import java.util.ArrayList;

/**
 * @author Renette
 * Fitness proportional selction for maximization problems
 */
public class FitnessMaxProportional<T extends Comparable<T>> implements Selection<T> {
    private ArrayList<Double> steps;
    private Generation<T> generation;
    @Override
    public Chromosome<T> select() {
        double r = Math.random();
        int i = 0;
        while (r > steps.get(i)) {
            ++i;
        }
        return generation.get(i);
    }

    public void setGeneration(Generation<T> generation) {
        this.generation = generation;
        generation.sort();
        ArrayList<Double> fitnessSteps = new ArrayList<>(generation.size());
        steps = new ArrayList<>(generation.size());
        double totalFitness = 0.0;
        for (Chromosome<T> t : generation) {
            final double fitness = t.getFitness();
            totalFitness += fitness;
            fitnessSteps.add(totalFitness);
        }
        for (Double fitness : fitnessSteps) {
            steps.add(fitness / totalFitness);
        }
    }
}
