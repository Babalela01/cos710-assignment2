package selection;

import ga.Chromosome;
import ga.Generation;

import java.util.ArrayList;
import java.util.stream.DoubleStream;


/**
 * @author Renette
 */
public interface Selection<T extends Comparable<T>> {

    /**
     * Selects and returns two traders from a generation
     *
     * @return Two traders.
     */
    Chromosome<T> select();

    void setGeneration(Generation<T> generation);

    static ArrayList<Double> roulette(double[] values) {
        ArrayList<Double> steps = new ArrayList<>(values.length);
        double total = DoubleStream.of(values).sum();
        double lastStep = 0;
        for (double val : values) {
            lastStep += val / total;
            steps.add(lastStep);
        }
        return steps;
    }
}
